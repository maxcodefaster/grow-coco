import { listenerCount } from 'events';
import * as p5 from 'p5';
import {PlantHealth} from '../@utils/data.service';

export class Batami {

  private readonly _p: p5;
  private readonly _id: number;
  private _health: PlantHealth;
  private readonly _modelAssets: BatamiModel;
  private readonly _others: Batami[];

  private _modelHolder;
  private _batTexture: p5.Image;

  private _bodyRadius: number = 90;

  private _touchedGround;
  private _isOnGround;

  private _posVec: p5.Vector;
  private _dirVec: p5.Vector;
  private _gravity: number;
  private _walkSpeed: number;

  constructor(p: p5, id: number, health: PlantHealth, model: BatamiModel, others: Batami[]) {
    this._p = p;
    this._id = id;
    this._health = health;
    this._modelAssets = model;
    this._others = others;
    this._touchedGround = false;
    this._isOnGround = false;
    this._posVec = p5.Vector.random3D();
    this._dirVec = p5.Vector.random2D();
    this._walkSpeed = this._p.random(1,3)
    this._gravity = 20;
  }

  setup() {
    this._modelHolder = this._p.loadModel(this._modelAssets.model);
    this._batTexture = this._p.loadImage(this._modelAssets.material[this._health]);
    this._dirVec = p5.Vector.random3D();
    this._posVec.x = this._p.random(-1200+this._bodyRadius, 1200-this._bodyRadius);
    this._posVec.y = this._p.random(-2000,-3200)
    this._posVec.z = this._p.random(-1200+this._bodyRadius, 1200-this._bodyRadius);
  }

  setPlantTexture(health: PlantHealth) {
    this._health = health;
    this._batTexture = this._p.loadImage(this._modelAssets.material[this._health]);
  }

  move() {
    this._posVec.add(this._dirVec.x * this._walkSpeed, this._gravity, this._dirVec.y * this._walkSpeed);
  }


  checkCollision() {
    for (let i = this._id + 1; i < this._others.length; i++) {
      let dx = this._others[i]._posVec.x - this._posVec.x;
      let dz = this._others[i]._posVec.z - this._posVec.z;
      let distance = Math.sqrt(dx * dx + dz * dz);
      let minDist = this._others[i]._bodyRadius + this._bodyRadius;

      if (distance < minDist) {
        let angle = Math.atan2(dz, dx);
        let targetX = this._posVec.x + Math.cos(angle) * minDist;
        let targetZ = this._posVec.z + Math.sin(angle) * minDist;
        this._dirVec.x -= targetX - this._others[i]._posVec.x;
        this._dirVec.y -= targetZ - this._others[i]._posVec.z;
        this._others[i]._dirVec.x += targetX - this._others[i]._posVec.x;
        this._others[i]._dirVec.y += targetZ - this._others[i]._posVec.z;
      }
      this._dirVec.limit(3);
      this._others[i]._dirVec.limit(3);
      this._walkSpeed = this._dirVec.mag();
      this._others[i]._walkSpeed = this._others[i]._dirVec.mag();
    }
  }

  checkBoundaries() {
    if(this._posVec.x >= 1200){
      this._posVec.x = 1200-1
      this._dirVec.x *= -1
    }
    else if(this._posVec.x <= -1200){
      this._posVec.x = -1200+1
      this._dirVec.x *= -1
    }
    else if(this._posVec.z >= 1200){
      this._posVec.z = 1200-1
      this._dirVec.y *= -1
    }
    else if(this._posVec.z <= -1200){
      this._posVec.z = -1200+1
      this._dirVec.y *= -1
    } 
    else if(this._posVec.y >= 0){
      this._posVec.y = -this._gravity
      this._touchedGround = true;
      this._isOnGround = true;
    }
  }

  show() {
    this._p.push();
    this._p.translate(this._posVec.x, this._posVec.y, this._posVec.z);
    this._p.rotateY(Math.atan2(this._posVec.x - (this._posVec.x + this._dirVec.x), this._posVec.z - (this._posVec.z + this._dirVec.y))+Math.PI);
    this._p.rotateZ(Math.sin(this._p.frameCount*0.2)*this._walkSpeed*0.09);
    this._p.texture(this._batTexture);
    this._p.model(this._modelHolder);
    this._p.pop();
  }
}


export class BatamiModel {
  name: string;
  material: {
    good: string,
    wet: string,
    dry: string,
  };
  model: string;

  constructor(name: string, material: { good: string, wet: string, dry: string }, body: string) {
    this.name = name;
    this.material = material;
    this.model = body;
  }
}
