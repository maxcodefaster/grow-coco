import {BatamiModel} from "./batami";

export const BatamiModelIndex: BatamiModel[] = [
  new BatamiModel(
    'Kresse',
    {
      good: 'assets/batami-models/kresse/T_Kresse.png',
      wet: 'assets/batami-models/kresse/T_Kresse_Wet.png',
      dry: 'assets/batami-models/kresse/T_Kresse_Dry.png'
    },
    'assets/batami-models/kresse/M_Kresse.obj'
  ),
  new BatamiModel(
    'Tomate',
    {
      good: 'assets/batami-models/tomate/T_Tomate.png',
      wet: 'assets/batami-models/tomate/T_Tomate_Wet.png',
      dry: 'assets/batami-models/tomate/T_Tomate_Dry.png'
    },
    'assets/batami-models/tomate/M_Tomate.obj',
  ),
  new BatamiModel(
    'Asparagus',
    {
      good: 'assets/batami-models/asparagus/T_Asparagus.png',
      wet: 'assets/batami-models/asparagus/T_Asparagus_Wet.png',
      dry: 'assets/batami-models/asparagus/T_Asparagus_Dry.png'
    },
    'assets/batami-models/asparagus/M_Asparagus.obj',
  )
]
