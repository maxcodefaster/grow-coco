import {PlantHealth} from "../@utils/data.service";

export class PlantData {

  private _temperature: number;
  private _airHumidity: number;
  private _soilHumidity: number;
  private _timeStamp: Date;
  private _healthStatus?: PlantHealth

  constructor(temperature: number, airHumidity: number, soilHumidity: number, timeStamp: Date, healthStatus?: PlantHealth) {
    this._temperature = temperature;
    this._airHumidity = airHumidity;
    this._soilHumidity = soilHumidity;
    this._timeStamp = timeStamp
    this._healthStatus = healthStatus
  }

  get temperature(): number {
    return this._temperature;
  }

  set temperature(value: number) {
    this._temperature = value;
  }

  get airHumidity(): number {
    return this._airHumidity;
  }

  set airHumidity(value: number) {
    this._airHumidity = value;
  }

  get soilHumidity(): number {
    return this._soilHumidity;
  }

  set soilHumidity(value: number) {
    this._soilHumidity = value;
  }

  get timeStamp(): Date {
    return this._timeStamp;
  }

  set timeStamp(value: Date) {
    this._timeStamp = value;
  }

  get healthStatus(): PlantHealth {
    return this._healthStatus
  }

  set healthStatus(value: PlantHealth) {
    this._healthStatus = value
  }
}
