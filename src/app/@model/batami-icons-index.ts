export const BatamiIcons = [
  {
    good: 'assets/batami-imgs/asparagus_good.png',
    wet: 'assets/batami-imgs/asparagus_wet.png',
    dry: 'assets/batami-imgs/asparagus_dry.png'
  }, {
    good: 'assets/batami-imgs/tomate_good.png',
    wet: 'assets/batami-imgs/tomate_wet.png',
    dry: 'assets/batami-imgs/tomate_dry.png'
  }, {
    good: 'assets/batami-imgs/kresse_good.png',
    wet: 'assets/batami-imgs/kresse_wet.png',
    dry: 'assets/batami-imgs/kresse_dry.png'
  },
]
