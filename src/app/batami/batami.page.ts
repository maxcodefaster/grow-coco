import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {Components} from '@ionic/core';
import {CanvasMatrixModalPage} from "../canvas-modal/canvas-matrix-modal-page.component";
import {ChatModalPage} from "../chat-modal/chat-modal-page.component";
import {DataService} from "../@utils/data.service";
import {PlantData} from "../@model/plant-data";
import {CalendarComponentOptions, DayConfig} from "ion2-calendar";
import {Subscription} from "rxjs";
import {UserSettingsService} from "../@utils/user-settings.service";

@Component({
  selector: 'app-immerse',
  templateUrl: 'batami.page.html',
  styleUrls: ['batami.page.scss']
})
export class BatamiPage implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription()

  plantData: PlantData[]
  users: string[]
  pokemon: string[]

  calLoading: boolean = false;
  receivingRealData: boolean = false;

  // calendar config
  calOptions: CalendarComponentOptions = {
    from: new Date().setDate(new Date().getDate() - 365),
    to: new Date(),
    weekStart: 1,
    showMonthPicker: false
  }

  constructor(
    private modalCtrl: ModalController,
    private data: DataService,
    private userSettings: UserSettingsService
  ) {
  }

  ngOnInit() {
    this.subscription.add(this.data.getRealData())
    this.subscription.add(this.sub2RealData())
    this.users = this.userSettings.getRandUserArr(2)
    this.pokemon = this.userSettings.getRandPokemonArr(1)
    this.getPlantData()
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  sub2RealData() {
    this.data.dataSubject.subscribe((data: PlantData) => {
      this.plantData[0] = data;
      this.calOptions.daysConfig[0].cssClass = data.healthStatus
      this.toggleRealData()
    })
  }

  toggleRealData() {
    if (!this.receivingRealData) {
      this.receivingRealData = true;
      this.calLoading = true;
      setTimeout(() => {
        this.calLoading = false;
      }, 1000);
    }
  }

  getPlantData() {
    const courses: string[] = ['CE1', 'CR1', 'DF1', 'PE1', 'PM1', 'CI1', 'CE2', 'CR2', 'DF2', 'PE2', 'PM2', 'CI2']
    let subTitle: string = courses[0]
    let daysConfig: DayConfig[] = []
    this.plantData = this.data.getDemoDataBatch(365)

    for (const [i, plantData] of this.plantData.entries()) {
      if (i % 14 == 0) subTitle = courses[Math.floor(Math.random() * courses.length)]

      daysConfig.push({
        date: plantData.timeStamp,
        cssClass: plantData.healthStatus,
        subTitle: subTitle
      });
      this.calOptions.daysConfig = daysConfig
    }
  }

  /* canvas matrix modal*/
  async openCanvas() {
    const canvasMatrixModal: Components.IonModal = await this.modalCtrl.create({
      component: CanvasMatrixModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await canvasMatrixModal.present();
  }

  /* chat modal*/
  async openChat() {
    const chat: Components.IonModal = await this.modalCtrl.create({
      component: ChatModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await chat.present();
  }

}
