import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BatamiPage } from './batami.page';

const routes: Routes = [
  {
    path: '',
    component: BatamiPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BatamiPageRoutingModule {}
