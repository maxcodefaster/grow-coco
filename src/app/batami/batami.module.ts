import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BatamiPage } from './batami.page';

import { BatamiPageRoutingModule } from './batami-routing.module';
import {ComponentsModule} from "../@components/components.module";
import {CalendarModule} from "ion2-calendar";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: BatamiPage }]),
    BatamiPageRoutingModule,
    ComponentsModule,
    CalendarModule
  ],
  declarations: [BatamiPage]
})
export class BatamiPageModule {}
