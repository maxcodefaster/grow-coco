import {Injectable} from '@angular/core';
import {AppSettingsService} from "../settings/app-settings-service";
import * as Pubnub from "pubnub";
import {Users} from "../@model/user";
import {Pokemon} from "../@model/pokemon";
import {PlantHealth} from "./data.service";
import {BatamiIcons} from "../@model/batami-icons-index";

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {

  constructor(private settings: AppSettingsService) {
  }

  async getUuid() {
    let uuid: string = await this.settings.userSettings.uuid.get()
    // generate and save uuid if not created yet
    if (uuid === '') {
      uuid = Pubnub.generateUUID()
      this.settings.userSettings.uuid.set(uuid)
    }
    return uuid
  }

  async getUserName() {
    let userName: string = await this.settings.userSettings.userName.get()
    // generate and save user name if not created yet
    if (userName === '') {
      userName = 'noName' + Math.floor(Math.random() * 9999)
      this.settings.userSettings.userName.set(userName)
    }
    return userName
  }

  setUserName(userName: string) {
    this.settings.userSettings.userName.set(userName)
  }

  getRandUserArr(amount: number = Users.length) {
    let a = Users;
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a.splice(0, amount);
  }

  getRandPokemonArr(amount: number = Pokemon.length) {
    let a = Pokemon;
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a.splice(0, amount);
  }

}
