import {Injectable} from '@angular/core';
import {webSocket, WebSocketSubject} from "rxjs/webSocket";
import {interval, Subject} from "rxjs";
import {PlantData} from "../@model/plant-data";
import {BatamiIcons} from "../@model/batami-icons-index";

const subject: WebSocketSubject<any> = webSocket({
  url: 'wss://cocobotanics.derstrudel.org:4000/?display',
  deserializer: ({data}) => data
});

export type PlantHealth = 'good' | 'dry' | 'wet'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public readonly dataSubject: Subject<PlantData> = new Subject<PlantData>()

  constructor() {
  }

  getRealData() {
    return subject.subscribe(
      msg => {
        const plantDataRaw: number[] = msg.split(',').map((e) => parseInt(e));
        const plantData: PlantData = new PlantData(
          plantDataRaw[0],
          plantDataRaw[1],
          plantDataRaw[2],
          new Date()
        )
        plantData.healthStatus = this.calcPlantHealth(plantData)
        this.dataSubject.next(plantData)
      },
      err => console.log(err),
      () => console.log('complete')
    );

  }

  getDemoData() {
    return interval(500).subscribe(() => {
      let newPlantData: PlantData = new PlantData(
        this.randomNumber(-15, 40),
        this.randomNumber(0, 100),
        this.randomNumber(1000, 5000),
        new Date()
      );
      newPlantData.healthStatus = this.calcPlantHealth(newPlantData)
      this.dataSubject.next(newPlantData)
    });
  }

  getDemoDataBatch(amount: number): PlantData[] {
    let data: PlantData[] = [];
    for (let i = 0; i < amount; i++) {
      // set last today - i date
      let date = new Date();
      date.setDate(date.getDate() - i)

      let newPlantData: PlantData = new PlantData(
        this.randomNumber(-15, 40),
        this.randomNumber(0, 100),
        this.randomNumber(1000, 5000),
        date
      );
      newPlantData.healthStatus = this.calcPlantHealth(newPlantData);
      data.push(newPlantData)
    }
    return data;
  }

  randomNumber(min: number, max: number): number {
    return Math.random() * (max - min) + min;
  }

  getRandPlantHealth() {
    let plantHealth: PlantHealth[] = ['good', 'dry', 'wet']
    let i: number = Math.round(this.randomNumber(0, 2))
    return plantHealth[i]
  }


  getRandBatamiIcon(health: PlantHealth) {
    let a = BatamiIcons;
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a[0][health];
  }

  calcPlantHealth(plantData: PlantData): PlantHealth {
    let plantHealth: PlantHealth = 'good'

    // check for plant stat and set health
    if (plantData.temperature < 5 || plantData.temperature > 25) plantHealth = 'dry'
    if (plantData.airHumidity < 10 || plantData.soilHumidity < 4000) plantHealth = 'dry'
    if (plantData.airHumidity > 70 || plantData.soilHumidity > 8000) plantHealth = 'wet'

    return plantHealth
  }
}
