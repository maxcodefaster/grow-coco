import { Observable, Subject } from 'rxjs';
import { Storage } from '@ionic/storage';

export class AppSettingsProperty<T> {
  propertySubject: Subject<T> = new Subject();
  type: string = 'unknown';
  prefix: string = 'v3';
  value: T = null;

  constructor(
    protected defaultVal: T,
    protected key: String,
    protected storageProvider: Storage,
    protected checkConstraint?: (value: T) => boolean,
    preventLoadDefault: boolean = false
  ) {
    this.type = typeof defaultVal;

    /* get current value from persistent storage for fast access */
    this.value = this.defaultVal;
    if (!preventLoadDefault) {
      this.get().then((c) => (this.value = c));
    }
  }

  /***
   * Saves value to persistent storage and emits an update event to all subscribers.
   */
  public set(value: T): Promise<any> {
    /* check constraints */
    if (this.checkConstraint && !this.checkConstraint(value)) {
      let errorMsg = `invalid settings value: ${this.key} - ${value}`;
      alert(errorMsg);
      throw errorMsg;
    }

    /* set new value and emit changed event */
    this.value = value;
    this.propertySubject.next(value);
    return this.storageProvider.set(`settings:${this.prefix}:${this.key}`, value);
  }

  /***
   * Returns value from persistent storage. Slow function, use only when needed.
   * Use .val() instead.
   */
  public async get(): Promise<T> {
    let ret = await this.storageProvider.get(`settings:${this.prefix}:${this.key}`);
    return ret != null ? ret : this.defaultVal;
  }

  /***
   * Removes the Settings key from persistent storage.
   */
  public remove(): Promise<any> {
    return this.storageProvider.remove(`settings:${this.prefix}:${this.key}`);
  }

  /***
   * Removes the Settings key from persistent storage.
   */
  public resetToDefault(): Promise<any> {
    return this.set(this.defaultVal);
  }

  public getTypeString() {
    return this.type;
  }

  public getKey() {
    return this.key;
  }

  /***
   * Returns a cached value of the setting. Fast processing time.
   * The value is updated on each .set() event.
   */
  public val(): T {
    return this.value;
  }

  /***
   * Subscribe to property changes. Immediately returns the current value.
   * Optimal for use in e.g @components to apply UI changes etc.
   */
  public watch(): Observable<T> {
    /* return current value, and changes */
    return new Observable((subscriber) => {
      /* get current value */
      this.get().then((currentValue) => subscriber.next(currentValue));

      return this.propertySubject.asObservable().subscribe((x) => {
        subscriber.next(x);
      });
    });
  }
}
