import { ComponentsModule } from '../@components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CanvasMatrixModalPage } from './canvas-matrix-modal-page.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
  ],
  declarations: [CanvasMatrixModalPage],
})
export class CanvasMatrixModalPageModule {}
