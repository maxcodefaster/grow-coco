import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {Components} from "@ionic/core";
import {ChatModalPage} from "../chat-modal/chat-modal-page.component";
import {CanvasMatrixModalPage} from "../canvas-modal/canvas-matrix-modal-page.component";

import * as p5 from 'p5';
import {PlantData} from "../@model/plant-data";
import {Subscription} from "rxjs";
import {DataService, PlantHealth} from "../@utils/data.service";
import {UserSettingsService} from "../@utils/user-settings.service";
import {Batami} from "../@model/batami";

@Component({
  selector: 'app-location',
  templateUrl: 'forest.page.html',
  styleUrls: ['forest.page.scss']
})
export class ForestPage implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription()

  plantData: PlantData[]
  batamis: [{ name: string, gardeners: string, img: string, health: string }] = [{
    name: '',
    gardeners: '',
    img: '',
    health: ''
  }]

  amount: number = 15;

  public constructor(
    private el: ElementRef,
    private modalCtrl: ModalController,
    private data: DataService,
    private userSettings: UserSettingsService
  ) {
  }

  ngOnInit() {
    this.plantData = this.data.getDemoDataBatch(365);
    this.genBatamis()
    // this.subscription.add(this.data.getDemoData())
    // this.subscription.add(this.sub2data())
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  sub2data() {
    this.data.dataSubject.subscribe((data: PlantData) => {
      console.log(data)
    })
  }

  genBatamis() {
    let users: string[] = this.userSettings.getRandUserArr()
    let pokemon: string[] = this.userSettings.getRandPokemonArr()
    this.batamis.shift()

    for (let i = 0; i < this.amount; i += 2) {
      let health: PlantHealth = this.data.getRandPlantHealth();
      this.batamis.push({
        name: pokemon[i],
        gardeners: users[i] + ' & ' + users[i + 1],
        img: this.data.getRandBatamiIcon(health),
        health
      })
    }

  }

  /* canvas matrix modal*/
  async openCanvas() {
    const canvasMatrixModal: Components.IonModal = await this.modalCtrl.create({
      component: CanvasMatrixModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await canvasMatrixModal.present();
  }


  /* chat modal*/
  async openChat() {
    const chat: Components.IonModal = await this.modalCtrl.create({
      component: ChatModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await chat.present();
  }

}
