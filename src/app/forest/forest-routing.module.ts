import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForestPage } from './forest.page';

const routes: Routes = [
  {
    path: '',
    component: ForestPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForestPageRoutingModule {}
