import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ForestPage} from './forest.page';

import {ForestPageRoutingModule} from './forest-routing.module';
import {ComponentsModule} from "../@components/components.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ForestPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ForestPage]
})
export class ForestPageModule {
}
