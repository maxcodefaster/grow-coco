import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'forest',
        loadChildren: () => import('../forest/forest.module').then(m => m.ForestPageModule)
      },
      {
        path: 'batami',
        loadChildren: () => import('../batami/batami.module').then(m => m.BatamiPageModule)
      },    
      {
        path: '',
        redirectTo: '/tabs/plant-canvas',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/batami',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
