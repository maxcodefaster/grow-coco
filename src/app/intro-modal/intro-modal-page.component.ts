import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, IonSlides, ToastController } from '@ionic/angular';
import { AppSettingsService } from '../settings/app-settings-service';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './intro-modal-page.component.html',
  styleUrls: ['./intro-modal-page.component.scss'],
})
export class IntroModalPage implements OnInit {
  @ViewChild('slides', { read: IonSlides, static: false }) slides: IonSlides;

  constructor(
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private settings: AppSettingsService,
  ) {
  }

  async ngOnInit() {
  }

  async dismissModal() {
    await this.settings.userSettings.introDone.set(true);
    await this.modalCtrl.dismiss({
      dismissed: true,
    });
  }

  async showToast(header: string, message: string) {
    const toast = await this.toastCtrl.create({
      header,
      message,
      duration: 4000,
      position: 'bottom',
    });
    await toast.present();
  }

  async next() {
    await this.slides.slideNext();
  }

  async prev() {
    await this.slides.slidePrev();
  }
}
