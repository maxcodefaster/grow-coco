import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {AppSettingsService} from "../settings/app-settings-service";
import {Components} from "@ionic/core";
import {IntroModalPage} from "../intro-modal/intro-modal-page.component";

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  constructor(
    private modalCtrl: ModalController,
    private settings: AppSettingsService) { }


  ngOnInit() {
  }

  async dismissModal() {
    this.modalCtrl.dismiss();
  }

  async introModal() {
    await this.settings.userSettings.introDone.set(false)
    const introModal: Components.IonModal = await this.modalCtrl.create({
      component: IntroModalPage,
      showBackdrop: false,
      cssClass: 'full-width-modal',
      componentProps: {}
    });
    await introModal.present();
  }

  scrollTo(elementId:string) {
    let el = document.getElementById(elementId);
    el.scrollIntoView({behavior: 'smooth'});
  }
  

}
