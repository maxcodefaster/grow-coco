import {OnInit, Component, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
import {AlertController, IonContent, ModalController} from '@ionic/angular';
import * as Pubnub from "pubnub";
import {UserSettingsService} from "../@utils/user-settings.service";

export class ChatMsg {
  constructor(public uuid: string, public author: string, public msg: string, public  dateCreated: string) {
  }
}

@Component({
  selector: 'app-chat-modal',
  templateUrl: './chat-modal-page.component.html',
  styleUrls: ['./chat-modal-page.component.scss'],
})
export class ChatModalPage implements OnInit, AfterViewInit {
  @ViewChild(IonContent) contentArea: IonContent;
  @ViewChild('chatList', {read: ElementRef}) chatList: ElementRef;
  occupancy: number = 0;

  // local vars
  chatMsgs: ChatMsg[] = [];
  message: string = '';
  mutationObserver: MutationObserver;
  userName: string

  // pubnub
  channel: string = 'chat';
  pubnub: Pubnub;
  uuid: string;


  constructor(
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private userSettings: UserSettingsService,
  ) {
  }

  async ngOnInit() {
    this.userName = await this.userSettings.getUserName()
    this.uuid = await this.userSettings.getUuid()
    this.initChat()
  }

  ngAfterViewInit() {
    this.mutationObserver = new MutationObserver((mutations) => {
      this.newChatAdded();
    });
    this.mutationObserver.observe(this.chatList.nativeElement, {
      childList: true
    });
  }

  async dismissModal() {
    this.modalCtrl.dismiss();
  }

  initChat() {
    /* PubNub */
    this.pubnub = new Pubnub({
      publishKey: 'pub-c-8e6a2b03-5b35-4fe9-ab79-71bc7119d563',
      subscribeKey: 'sub-c-b7058f3c-2d67-11eb-9713-12bae088af96',
      ssl: true,
      uuid: this.uuid
    });

    this.pubnub.subscribe({
      channels: [this.channel],
      withPresence: true
    });

    this.pubnub.addListener({
      message: (event) => {
        this.chatMsgs.push(event.message.content);
      },
      presence: (event) => {
        this.occupancy = event.occupancy
      }
    })

    this.pubnub.history(
      {
        channel: this.channel,
        count: 50,
        stringifiedTimeToken: true,
      },
      (status, response) => {
        if (response) {
          for (let msg of response.messages) {
            this.chatMsgs.push(msg.entry.content);
          }
        }
      }
    );
  }

  publish(data) {
    this.pubnub.publish({
      channel: this.channel,
      message: {"sender": this.uuid, "content": data}
    });
  }

  newChatAdded(): void {
    setTimeout(() => {
      if (this.contentArea.scrollToBottom) {
        this.contentArea.scrollToBottom(400);
      }
    }, 500);
  }

  preventDefault(event): void {
    event.preventDefault();
  }

  addChat(): void {
    if (this.message.length > 0) {
      const msg: ChatMsg = new ChatMsg(this.uuid, this.userName, this.message, new Date().toISOString())
      this.publish(msg)
      this.message = '';
    }
  }

  async changeUserName() {

    const alert = await this.alertCtrl.create({
      header: 'Change your user name',
      inputs: [
        {
          name: 'userName',
          type: 'text',
          placeholder: this.userName,
          attributes: {
            maxlength: 8,
          }
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Ok',
          handler: (input) => {
            this.userName = input.userName
            this.userSettings.setUserName(this.userName)
          }
        }
      ]
    });
    await alert.present();

  }


}
