import {Component, Input, OnInit} from '@angular/core';
import {Platform} from "@ionic/angular";
import {Components} from "@ionic/core";
import {AboutPage} from "../../about/about.page";
import {ModalController} from "@ionic/angular";
import {IntroModalPage} from "../../intro-modal/intro-modal-page.component";
import {AppSettingsService} from "../../settings/app-settings-service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() activeSite: string = '';
  isMobile: boolean = false

  constructor(
    private platform: Platform,
    private modalCtrl: ModalController,
    private settings: AppSettingsService
  ) {
  }

  async ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
    if (!await this.settings.userSettings.introDone.get()) this.openIntro()
  }

  async openAbout() {
    const AboutModal: Components.IonModal = await this.modalCtrl.create({
      component: AboutPage,
      showBackdrop: false,
      componentProps: {},
      cssClass: 'full-width-modal'
    });
    await AboutModal.present();
  }

  /* intro modal*/
  async openIntro() {
    const introModal: Components.IonModal = await this.modalCtrl.create({
      component: IntroModalPage,
      showBackdrop: false,
      cssClass: 'full-width-modal',
      componentProps: {}
    });
    await introModal.present();
  }
}
