import {AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import * as p5 from "p5";
import {Batami} from "../../@model/batami";
import {BatamiModelIndex} from "../../@model/batami-model-index";
import {PlantData} from "../../@model/plant-data";

@Component({
  selector: 'app-plant-canvas',
  templateUrl: './plant-canvas.component.html',
  styleUrls: ['./plant-canvas.component.scss'],
})
export class PlantCanvasComponent implements AfterViewInit, OnInit, OnChanges {
  @Input() plantData: PlantData[];
  @Input() amount: number;
  @Input() receivingRealData?: boolean;

  @ViewChild('p5CanvasContainer', {read: ElementRef, static: true}) p5CanvasContainer: ElementRef;

  canvasSizeX: number = 200;
  canvasSizeY: number = 200;

  // p5 instance
  p: p5;

  //local vars
  batamis: Batami[] = [];
  bg: p5.Image

  public constructor(
    private el: ElementRef,
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const p5obj = new p5((p: p5) => {
      this.p = p;
      this.p.setup = () => {
        this.setup();
      };
      this.p.draw = () => {
        this.draw();
      };
      this.p.windowResized = () => {
        this.resize()
      };
      this.p.preload = () => {
        this.preload()
      }
    }, this.el.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges) {
    // change batami health on real data input
    if(changes.receivingRealData && this.batamis[0]) {
      this.batamis[0].setPlantTexture(this.plantData[0].healthStatus)
    }
  }

  resize() {
    this.canvasSizeX = this.p5CanvasContainer.nativeElement.offsetWidth;
    this.canvasSizeY = this.p5CanvasContainer.nativeElement.offsetWidth;
    this.p.resizeCanvas(this.canvasSizeX, this.canvasSizeY);
    this.p.camera(-2400, -1500, 2400, 0, 0, 0, 0, 1, 0);
    this.p.ortho(-this.canvasSizeX*2.4, this.canvasSizeX*2.4, -this.canvasSizeY*2.4, this.canvasSizeY*2.4, 0, 7000);
  }

  preload() {
    // setup bg
    this.bg = this.p.loadImage("/assets/imgs/forest_bg.png");

    this.genBatamis(this.amount)
  }

  setup() {
    this.p.createCanvas(this.canvasSizeX, this.canvasSizeY, this.p.WEBGL)
      .parent(this.p5CanvasContainer.nativeElement);
    // resize canvas on init
    this.resize()
    

    this.p.noStroke()
    //this.p.angleMode('degrees')
    for (let batami of this.batamis) {
      batami.setup()
    }
    this.p.camera(-2400, -1500, 2400, 0, 0, 0, 0, 1, 0);
    this.p.ortho(-this.canvasSizeX*2.4, this.canvasSizeX*2.4, -this.canvasSizeY*2.4, this.canvasSizeY*2.4, 0, 7000);
  }

  draw() {
    // if canvas not properly sized
    if (this.p.width === 0 || this.p.height === 0) {
      this.resize()
    }
    
    this.p.ortho(-this.canvasSizeX*2.4, this.canvasSizeX*2.4, -this.canvasSizeY*2.4, this.canvasSizeY*2.4, 0, 7000);
    this.drawBG()

    for (let batami of this.batamis) {
      batami.checkCollision();
      batami.move()
      batami.show()
      batami.checkBoundaries(); 
    }

  }

  genBatamis(amount: number) {
    for (let i = 0; i < amount; i++) {
      this.batamis[i] = new Batami(
        this.p,
        i,
        this.plantData[i].healthStatus,
        BatamiModelIndex[Math.floor(Math.random() * BatamiModelIndex.length)],
        this.batamis
      );
    }
  }


  drawBG() {
    this.p.imageMode(this.p.CENTER);
    this.p.push()
    this.p.rotateY(-Math.PI/4);
    this.p.translate(0, 700, -2100);

    this.p.image(this.bg,0,0,this.canvasSizeX*6.20,this.canvasSizeY*6.41);
    this.p.pop()

    //this.p.background(255, 255, 255, 0)
    this.p.ambientLight(170, 170, 170)
    this.p.directionalLight(255, 170, 170, 0.25, 0.25, 0)
  }

}
