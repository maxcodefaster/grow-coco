import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';

import {FormsModule} from '@angular/forms';
import {HeaderComponent} from "./header/header.component";
import {PlantCanvasComponent} from "./plant-canvas/plant-canvas.component";

@NgModule({
  declarations: [
    HeaderComponent,
    PlantCanvasComponent,
  ],
  imports: [CommonModule, IonicModule, FormsModule],
  exports: [
    HeaderComponent,
    PlantCanvasComponent,
  ]
})
export class ComponentsModule {
}
