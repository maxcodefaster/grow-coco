import {Component } from '@angular/core';
import {Title, Meta} from '@angular/platform-browser';

import { Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private titleService: Title,
    private metaService: Meta,
  ) {
    this.initializeApp();
    this.addSeo()
  }

  initializeApp() {
    const isNative = this.platform.is('android') || this.platform.is('ios');
    this.platform.ready().then(() => {
      if (isNative) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }
    });
  }

  addSeo() {
    this.titleService.setTitle('GrowCoCo');
    this.metaService.updateTag({name: 'description', content: 'The On Site App of Code & Context'});
  }

}
